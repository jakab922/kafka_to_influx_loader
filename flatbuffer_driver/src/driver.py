import logging
import subprocess
import sys
from datetime import datetime
from json import dump, dumps
from os import environ
from random import choice, randint, random
from time import sleep
from typing import Dict, Optional, Tuple

from confluent_kafka import KafkaError, Message, Producer

log = logging.getLogger()

terms = [
    "AUG20",
    "SEP22",
    "JAN21",
    "DEC20",
]

portfolio_names = [
    "Blue",
    "Red",
    "Rio",
    "White",
    "Maple",
    "Samba"
]

symbol_names = [
    "AC",
    "AH",
    "CO",
    "UL",
    "UN",
    "ABN",
    "AEX",
    "AGN",
    "AKZ",
    "AMG",
    "ASM",
]

generated_json = "generated.json"
generated_bin = "generated.bin"
kafka_data_fbs = "kafka_data.fbs"


def setup_logging() -> None:
    log.setLevel(logging.INFO)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)
    log.addHandler(handler)


def generate_data() -> Dict:
    now = datetime.utcnow()
    vols = [random() * 1000 for _ in range(29)]
    return {
        "rec_ts": int(datetime.timestamp(now) * 10 ** 6),
        "rec_dt": randint(0, 1),  # Unused
        "portfolio": choice(portfolio_names),
        "symbol": choice(symbol_names),
        "term": choice(terms),
        "tte": random() * 10,
        "fwd": random() * 10 ** 6,
        "curve_id": randint(0, 1),  # Unused
        "vols": vols,
        "usable": choice([True, False]),
        "svol": choice([True, False]),
    }


def encode_data() -> None:
    subprocess.call(["flatc", "-b", kafka_data_fbs, generated_json])


def setup() -> Tuple[Producer, str]:
    setup_logging()
    kafka_host = environ.get("KAFKA_HOST")
    kafka_port = environ.get("KAFKA_PORT")
    kafka_address = f"{kafka_host}:{kafka_port}"
    kafka_topic = environ.get("KAFKA_TOPIC")

    producer_config = {"bootstrap.servers": kafka_address}

    producer = Producer(producer_config)

    return (producer, kafka_topic)


def report(error: Optional[KafkaError], message: Message) -> None:
    if error is not None:
        log.error(f"Error happened during delivery {error}")
    else:
        log.info(
            f"Message delivered to topic {message.topic()}[{message.partition()}]: {message.value()}"
        )


def main() -> None:
    producer, kafka_topic = setup()

    while True:
        generated_data = generate_data()
        with open(generated_json, "w") as f:
            dump(generated_data, f)

        encode_data()

        with open(generated_bin, "rb") as f:
            data = f.read()

        producer.poll(0)
        producer.produce(kafka_topic, data, callback=report)
        producer.flush()
        sleep(0.5 * random() * 1.0)


if __name__ == "__main__":
    main()
