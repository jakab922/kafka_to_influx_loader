from confluent_kafka import Consumer, Message
from influxdb import InfluxDBClient
from os import environ
import logging
import sys
from typing import Optional, Dict
from dataclasses import dataclass
from datetime import datetime
from json import dumps
from kafka_data.TermImpliedVol import TermImpliedVol

log = logging.getLogger()

volatility_values = [25, 50, 75, 100, 125, 150,
                     175, 200, 225, 250, 275, 300, 400, 500]
volatility_names = (
    [f"vol_minus_{value}" for value in volatility_values[::-1]]
    + ["atm_vol"]
    + [f"vol_plus_{value}" for value in volatility_values]
)

volatility_translation = {
    volatility_name: volatility_name[:-3] for volatility_name in volatility_names
}

translation = {
    "rec_ts": "timestamp",
    "portfolio": "portfolio",
    "symbol": "symbol",
    "term": "term",
    "tte": "tte",
    "fwd": "fwd",
    "usable": "usable",
    "svol": "is_svol",
}


def string_decoder(s: bytes) -> str:
    return s.decode("utf-8")


postprocess = {
    "term": string_decoder,
    "portfolio": string_decoder,
    "symbol": string_decoder
}

tags = {"portfolio", "symbol", "term", "usable", "is_svol"}


def camel(s: str) -> str:
    return "".join(elem.capitalize() for elem in s.split("_"))


def decamel(s: str) -> str:
    ret = []
    for i, c in enumerate(s):
        if c.isupper() and i != 0:
            ret.extend(["_", c.lower()])
        else:
            ret.append(c)
    return "".join(ret)


def setup_logging() -> None:
    log.setLevel(logging.INFO)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler.setFormatter(formatter)
    log.addHandler(handler)


def transform_raw_data_to_json(raw_data: bytes) -> Dict:
    term_implied_volatility = TermImpliedVol.GetRootAsTermImpliedVol(
        raw_data, 0)
    ret = {
        value: getattr(term_implied_volatility, camel(key))()
        for key, value in translation.items()
    }
    volatility_values = [
        term_implied_volatility.Vols(i)
        for i in range(term_implied_volatility.VolsLength())
    ]
    for value, name in zip(volatility_values, volatility_names):
        ret[name] = value
    for key, f in postprocess.items():
        ret[key] = f(ret[key])
    return ret


def json_data_to_influx_data(json_data: Dict, influxdb_measurement: str) -> Dict:
    ret = {
        "measurement": influxdb_measurement,
        "tags": dict(),
        "time": "",
        "fields": dict(),
    }
    for key, value in json_data.items():
        if key in tags:
            ret["tags"][key] = str(value)
        elif key == "timestamp":
            ret["time"] = value
        else:
            ret["fields"][key] = value

    return ret


def run_loader(
    consumer: Consumer, influxdb_client: InfluxDBClient, influxdb_measurement: str
) -> None:
    while True:
        message: Optional[Message] = consumer.poll(1)

        if message is None:
            log.info("No message")
            continue

        if message.error():
            log.error(
                f"An error happened while trying to consume a message from Kafka: {message.error()}"
            )
            continue

        log.info(
            f"Relaying the following message from Kafka to InfluxDB: {message.value()}"
        )

        json_data = transform_raw_data_to_json(message.value())
        influx_data = json_data_to_influx_data(json_data, influxdb_measurement)

        log.info(
            f"Writing the following data to InfluxDB: {dumps(influx_data, sort_keys=True, indent=4)}"
        )
        # influxdb_client.write_points([influx_data])  # TODO: re-enable this. I just want to see if everything works first.


if __name__ == "__main__":
    setup_logging()
    kafka_host = environ.get("KAFKA_HOST")
    kafka_port = environ.get("KAFKA_PORT")
    kafka_address = f"{kafka_host}:{kafka_port}"
    kafka_topic = environ.get("KAFKA_TOPIC")
    kafka_consumer_group = environ.get("KAFKA_CONSUMER_GROUP")
    kafka_offset = environ.get(
        "KAFKA_OFFSET", "earliest"
    )  # TODO: Need to figure out a way to continue from where we left off.
    influxdb_host = environ.get("INFLUXDB_HOST")
    influxdb_port = environ.get("INFLUXDB_PORT")
    influxdb_database = environ.get("INFLUXDB_DATABASE")
    influxdb_measurement = environ.get("INFLUXDB_MEASUREMENT")

    consumer_config = {
        "bootstrap.servers": kafka_address,
        "group.id": kafka_consumer_group,
        "auto.offset.reset": "earliest",
    }

    consumer = Consumer(consumer_config)
    consumer.subscribe([kafka_topic])

    influxdb_client = InfluxDBClient(
        host=influxdb_host, port=influxdb_port, database=influxdb_database, retries=0
    )
    influxdb_client.create_database(influxdb_database)

    run_loader(consumer, influxdb_client, influxdb_measurement)
