# Demo project for loading flatbuffer data from Kafka to InfluxDB

## Prerequisites

You will need `git`, `docker` and `docker-compose` to work with this repository

## How to start this

You can start the project by navigating to the root folder and issuing `docker-compose up` assuming the docker daemon
is already started on your system.

## Description of the components

We have a driver defined in the `flatbuffer_driver` folder which is generating flatbuffer encoded data
for Kafka. 

Then we also have a component called loader for which the code is located in the `loader` directory.
This one listens to a Kafka topic and unpacks the flatbuffer encoded data, transforms it and loads it
into InfluxDB.

Then we have several components for which we use ready made docker images and we only configure them. 
These are the following:

- `zookeeper`: This is a necessary component for Kafka.
- `kafka:9092`: This is simulating the firehose Kafka we want to read the flatbuffer encoded data from.
- `influxdb:8086`: This is our time-series database of choice. We will store the hot data(as in the last week) in this database.
- `chronograf:8888`: This is the UI provided by Influxdata the developers of InfluxDB. Currently the only UI where you can use the [Flux](https://docs.influxdata.com/flux/v0.65/) query language which we plan to use on the long run.
- `grafana:3000`: The UI we plan to use. Currently version `6.7.4` is part of this project. There is a [plugin](https://grafana.com/grafana/plugins/grafana-influxdb-flux-datasource) for `7.0.0+` which would let you use Flux with Grafana but it's in beta and it's not working.